from flask_wtf import FlaskForm
from wtforms import (
    IntegerField,
    PasswordField,
    SubmitField,
    BooleanField,
    StringField,
    TextAreaField,
    SelectField 
    
)

from wtforms.validators import (
    DataRequired,
    Length,
    Email,
    EqualTo,
    ValidationError,
    email_validator,
    NumberRange
)



class ComposeMailForm(FlaskForm):
    recipient_id = IntegerField('Recipient ID', validators=[DataRequired(), NumberRange(min=1100, max=8799)])
    subject = StringField("Subject", validators=[DataRequired()])
    priority = SelectField('Priority', choices=[('high', 'High'), ('average', 'Average'), ('low', 'Low')], validators=[DataRequired()])
    synopsys = StringField('Write a brief overview', validators=[DataRequired()])
    content = TextAreaField('Mail content...', validators=[DataRequired()])
    send = SubmitField('Send')
    draft = SubmitField('Draft')
    cancel = SubmitField('Cancel')
    

class UpdateMailForm(FlaskForm):
    close = SubmitField('Close')
    revert = SubmitField('Revert')
    archive = SubmitField('Archive')
    mark_read = SubmitField('Mark as read')
    