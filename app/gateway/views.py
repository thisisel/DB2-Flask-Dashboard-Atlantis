from flask import render_template, request, url_for, flash, redirect
from flask_login import login_user, current_user, logout_user, login_required
from .forms import LoginForm
from .. import bcrypt, db
from ..models import VPersonnel
from . import gateway


@gateway.route("/",methods=["GET", "POST"])
@gateway.route("/login", methods=["GET", "POST"])
def login():

    if current_user.is_authenticated:
        return redirect(url_for("dashboard.homepage"))

    form = LoginForm()

    if form.validate_on_submit():
        # personnel = TPersonnel.query.filter_by(id=form.p_id.data).first()
        print(f'for data for id  is {form.id.data}')
        personnel = db.session.query(VPersonnel).filter_by(id = form.id.data).first()
        test = db.session.query(VPersonnel).filter_by(id = 5140).first()
        print(test)
        print(personnel)
        if personnel and bcrypt.check_password_hash(
            personnel.prsnl_password, form.password.data
        ):
            login_user(personnel, remember=form.remember.data)
            next_page = request.args.get("next")
            print('LOOGED in')
            return redirect(next_page) if next_page else render_template("dashboard/dashboard_base.html")
        else:
             print("Login Unsuccessful. Please check email and password")
    return render_template("gateway/signin.html", form=form)


@gateway.route("/logout")
def logout():
    logout_user()
    print('LOGGED OUT')
    return redirect(url_for("gateway.login"))
