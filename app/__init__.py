# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_login import LoginManager

db = SQLAlchemy()

bcrypt = Bcrypt()

login_manager = LoginManager()


def create_app():
    app = Flask(__name__)
    app.config.from_object('config.Config')

    db.init_app(app)

    bcrypt.init_app(app)    
    login_manager.init_app(app)
    login_manager.login_view = 'gateway.login'
    login_manager.login_message_category = 'info'

    with app.app_context():
        from . import models
        from . import gateway
        from . import dashboard

        from .gateway import gateway as gateway_bp
        app.register_blueprint(gateway_bp, url_prefix='')
            
        from .dashboard import dashboard as dashboard_bp
        app.register_blueprint(dashboard_bp, url_prefix='/dashboard')

        return app
